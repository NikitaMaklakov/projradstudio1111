//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.Dialogs.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Media.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdActns.hpp>
#include <IPPeerClient.hpp>
#include <IPPeerServer.hpp>
#include <System.Actions.hpp>
#include <System.Tether.AppProfile.hpp>
#include <System.Tether.Manager.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TTrackBar *TrackBar1;
	TLayout *Layout1;
	TLabel *Label1;
	TMemo *Memo1;
	TLayout *Layout2;
	TTrackBar *TrackBar2;
	TLabel *Label2;
	TMediaPlayer *MediaPlayer1;
	TActionList *ActionList1;
	TOpenDialog *OpenDialog1;
	TTetheringManager *TetheringManager1;
	TTetheringAppProfile *TetheringAppProfile1;
	TMediaPlayerStop *MediaPlayerStop1;
	TMediaPlayerPlayPause *MediaPlayerPlayPause1;
	TMediaPlayerCurrentTime *MediaPlayerCurrentTime1;
	TMediaPlayerVolume *MediaPlayerVolume1;
	TMediaPlayerControl *MediaPlayerControl1;
	TAction *acClear;
	TAction *acPlayPause;
	TAction *acStop;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
