//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ManClick(TObject *Sender)
{
Form2->Show();
Form2->Viewport3D1->Visible = true;
Form2->Viewport3D2->Visible = false;
Form2->CheckBox2->Visible = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::SphereClick(TObject *Sender)
{
Form2->Show();
Form2->Viewport3D1->Visible = false;
Form2->Viewport3D2->Visible = true;
Form2->Sphere1->Visible = true;
Form2->CheckBox2->Visible = true;

}
//---------------------------------------------------------------------------
