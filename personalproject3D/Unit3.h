//---------------------------------------------------------------------------

#ifndef Unit3H
#define Unit3H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Controls3D.hpp>
#include <FMX.MaterialSources.hpp>
#include <FMX.Objects3D.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Types3D.hpp>
#include <FMX.Viewport3D.hpp>
#include <System.Math.Vectors.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Colors.hpp>
#include <FMX.ListBox.hpp>
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
	TViewport3D *Viewport3D1;
	TModel3D *Model3D1;
	TLightMaterialSource *Model3D1Mat01;
	TLightMaterialSource *Model3D1Mat11;
	TLightMaterialSource *Model3D1Mat21;
	TLightMaterialSource *Model3D1Mat31;
	TLightMaterialSource *Model3D1Mat41;
	TLightMaterialSource *Model3D1Mat51;
	TLight *Light1;
	TToolBar *ToolBar1;
	TTextureMaterialSource *TextureMaterialSource1;
	TViewport3D *Viewport3D2;
	TSphere *Sphere1;
	TTrackBar *TrackBar1;
	TFloatAnimation *Anim1;
	TDummy *Dummy1;
	TCheckBox *CheckBox1;
	TFloatAnimation *Anim2;
	TDummy *Dummy2;
	TStyleBook *StyleBook1;
	TGridPanelLayout *GridPanelLayout1;
	TLabel *Label1;
	TTrackBar *tbSize;
	TLabel *Label2;
	TButton *bureset;
	TTextureMaterialSource *TextureMaterialSource2;
	TCheckBox *CheckBox2;
	void __fastcall TrackBar1Change(TObject *Sender);
	void __fastcall CheckBox1Change(TObject *Sender);
	void __fastcall tbSizeChange(TObject *Sender);
	void __fastcall buresetClick(TObject *Sender);
	void __fastcall CheckBox2Change(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
