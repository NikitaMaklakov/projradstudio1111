//----------------------------------------------------------------------------

#ifndef ClientModuleUnit1H
#define ClientModuleUnit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "ClientClassesUnit1.h"
#include <Data.DB.hpp>
#include <Data.DBXCommon.hpp>
#include <Data.DBXDataSnap.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.DSConnect.hpp>
#include <IPPeerClient.hpp>
//----------------------------------------------------------------------------
class TClientModule1 : public TDataModule
{
__published:	// IDE-managed Components
	TSQLConnection *SQLConnection1;
	TDSProviderConnection *DSProviderConnection1;
	TClientDataSet *ClientDataSet1;
	TClientDataSet *ClientDataSet2;
	TSmallintField *ClientDataSet1ID;
	TWideStringField *ClientDataSet1NAME_SERVICE;
	TWideStringField *ClientDataSet1NAME_MED;
	TSmallintField *ClientDataSet2ID;
	TWideStringField *ClientDataSet2NAME_SERVICE;
	TWideStringField *ClientDataSet2NAME_MED;
	TWideStringField *ClientDataSet2NAME_YOUR;
	TIntegerField *ClientDataSet2IDENT_CODE;
	TDateField *ClientDataSet2DATE;
	TTimeField *ClientDataSet2TIME;
private:	// User declarations
	bool FInstanceOwner;
	TServerMethods1Client* FServerMethods1Client;
	TServerMethods1Client* GetServerMethods1Client(void);
public:		// User declarations
	__fastcall TClientModule1(TComponent* Owner);
	__fastcall ~TClientModule1();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TServerMethods1Client* ServerMethods1Client = {read=GetServerMethods1Client, write=FServerMethods1Client};
};
//---------------------------------------------------------------------------
extern PACKAGE TClientModule1 *ClientModule1;
//---------------------------------------------------------------------------
#endif
