//---------------------------------------------------------------------------

#ifndef ServerMethodsUnit1H
#define ServerMethodsUnit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class TServerMethods1 : public TDSServerModule
{
__published:	// IDE-managed Components
	TSQLConnection *HospitalConnection;
	TSQLDataSet *HospitalTable;
	TSQLDataSet *ReceptionTable;
	TSQLStoredProc *Reception_insProc;
	TDataSetProvider *dspHospital;
	TDataSetProvider *dspReception;
	TSmallintField *HospitalTableID;
	TWideStringField *HospitalTableNAME_SERVICE;
	TWideStringField *HospitalTableNAME_MED;
	TSmallintField *ReceptionTableID;
	TWideStringField *ReceptionTableNAME_SERVICE;
	TWideStringField *ReceptionTableNAME_MED;
	TWideStringField *ReceptionTableNAME_YOUR;
	TIntegerField *ReceptionTableIDENT_CODE;
	TDateField *ReceptionTableDATE;
	TTimeField *ReceptionTableTIME;
private:	// User declarations
public:		// User declarations
	__fastcall TServerMethods1(TComponent* Owner); 
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
};
//---------------------------------------------------------------------------
extern PACKAGE TServerMethods1 *ServerMethods1;
//---------------------------------------------------------------------------
#endif

