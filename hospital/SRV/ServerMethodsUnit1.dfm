object ServerMethods1: TServerMethods1
  OldCreateOrder = False
  Height = 250
  Width = 453
  object HospitalConnection: TSQLConnection
    ConnectionName = 'hospital'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'GetDriverFunc=getSQLDriverINTERBASE'
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      'Database=C:\Users\VD\Desktop\DataBase\DATABASE.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    Connected = True
    Left = 57
    Top = 49
  end
  object HospitalTable: TSQLDataSet
    Active = True
    CommandText = 'HOSPITAL'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = HospitalConnection
    Left = 129
    Top = 82
    object HospitalTableID: TSmallintField
      FieldName = 'ID'
      Required = True
    end
    object HospitalTableNAME_SERVICE: TWideStringField
      FieldName = 'NAME_SERVICE'
      Required = True
      Size = 800
    end
    object HospitalTableNAME_MED: TWideStringField
      FieldName = 'NAME_MED'
      Required = True
      Size = 800
    end
  end
  object ReceptionTable: TSQLDataSet
    Active = True
    CommandText = 'RECEPTION'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = HospitalConnection
    Left = 191
    Top = 82
    object ReceptionTableID: TSmallintField
      FieldName = 'ID'
      Required = True
    end
    object ReceptionTableNAME_SERVICE: TWideStringField
      FieldName = 'NAME_SERVICE'
      Required = True
      Size = 800
    end
    object ReceptionTableNAME_MED: TWideStringField
      FieldName = 'NAME_MED'
      Required = True
      Size = 800
    end
    object ReceptionTableNAME_YOUR: TWideStringField
      FieldName = 'NAME_YOUR'
      Required = True
      Size = 800
    end
    object ReceptionTableIDENT_CODE: TIntegerField
      FieldName = 'IDENT_CODE'
      Required = True
    end
    object ReceptionTableDATE: TDateField
      FieldName = 'DATE'
      Required = True
    end
    object ReceptionTableTIME: TTimeField
      FieldName = 'TIME'
      Required = True
    end
  end
  object Reception_insProc: TSQLStoredProc
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftString
        Precision = 200
        Name = 'NAME_SERVICE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Precision = 200
        Name = 'NAME_MED'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Precision = 200
        Name = 'NAME_YOUR'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Precision = 4
        Name = 'IDENT_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Precision = 4
        Name = 'DATE'
        ParamType = ptInput
      end
      item
        DataType = ftTime
        Precision = 4
        Name = 'TIME'
        ParamType = ptInput
      end>
    SQLConnection = HospitalConnection
    StoredProcName = 'RECEPTION_INS'
    Left = 425
    Top = 126
  end
  object dspHospital: TDataSetProvider
    DataSet = HospitalTable
    Left = 104
    Top = 160
  end
  object dspReception: TDataSetProvider
    DataSet = ReceptionTable
    Left = 200
    Top = 160
  end
end
