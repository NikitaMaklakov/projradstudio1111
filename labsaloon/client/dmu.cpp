//----------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <memory>

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
	FInstanceOwner = true;
}

__fastcall Tdm::~Tdm()
{
	delete FServerMethods1Client;
}

TServerMethods1Client* Tdm::GetServerMethods1Client(void)
{
	if (FServerMethods1Client == NULL)
	{
		SQLConnection1->Open();
		FServerMethods1Client = new TServerMethods1Client(SQLConnection1->DBXConnection, FInstanceOwner);
	}
	return FServerMethods1Client;
};

