//---------------------------------------------------------------------------

#ifndef labhH
#define labhH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls3D.hpp>
#include <FMX.Objects3D.hpp>
#include <FMX.Types.hpp>
#include <FMX.Viewport3D.hpp>
#include <System.Math.Vectors.hpp>
#include <FMX.Ani.hpp>
#include <FMX.MaterialSources.hpp>
//---------------------------------------------------------------------------
class TForm5 : public TForm
{
__published:	// IDE-managed Components
	TViewport3D *Viewport3D1;
	TDummy *Dummy1;
	TGrid3D *Grid3D1;
	TFloatAnimation *FloatAnimation1;
	TPath3D *Path3D1;
	TPath3D *Path3D2;
	TPath3D *Path3D3;
	TPath3D *Path3D4;
	TLightMaterialSource *LightMaterialSource1;
	TLightMaterialSource *LightMaterialSource2;
	TPath3D *Path3D5;
	TLightMaterialSource *LightMaterialSource3;
private:	// User declarations
public:		// User declarations
	__fastcall TForm5(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm5 *Form5;
//---------------------------------------------------------------------------
#endif
