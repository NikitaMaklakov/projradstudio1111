object ClientModule1: TClientModule1
  OldCreateOrder = False
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/')
    Connected = True
    Left = 48
    Top = 40
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'TServerMethods1'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 192
    Top = 120
  end
  object ClientDataSet1: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dspHospital'
    RemoteServer = DSProviderConnection1
    Left = 168
    Top = 184
    object ClientDataSet1ID: TSmallintField
      FieldName = 'ID'
      Required = True
    end
    object ClientDataSet1NAME_SERVICE: TWideStringField
      FieldName = 'NAME_SERVICE'
      Required = True
      Size = 800
    end
    object ClientDataSet1NAME_MED: TWideStringField
      FieldName = 'NAME_MED'
      Required = True
      Size = 800
    end
  end
  object ClientDataSet2: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dspReception'
    RemoteServer = DSProviderConnection1
    Left = 232
    Top = 192
    object ClientDataSet2ID: TSmallintField
      FieldName = 'ID'
      Required = True
    end
    object ClientDataSet2NAME_SERVICE: TWideStringField
      FieldName = 'NAME_SERVICE'
      Required = True
      Size = 800
    end
    object ClientDataSet2NAME_MED: TWideStringField
      FieldName = 'NAME_MED'
      Required = True
      Size = 800
    end
    object ClientDataSet2NAME_YOUR: TWideStringField
      FieldName = 'NAME_YOUR'
      Required = True
      Size = 800
    end
    object ClientDataSet2IDENT_CODE: TIntegerField
      FieldName = 'IDENT_CODE'
      Required = True
    end
    object ClientDataSet2DATE: TDateField
      FieldName = 'DATE'
      Required = True
    end
    object ClientDataSet2TIME: TTimeField
      FieldName = 'TIME'
      Required = True
    end
  end
end
